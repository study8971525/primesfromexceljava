package org.example;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * This program reads and validates data from a xlsx file, which is located on the first sheet in column B.
 * It checks if the data are numeric values and, if so, tests whether the number is a prime number or not.
 * If a prime number is found, it is printed on a separate line in the console.
 * The program is designed to work efficiently without loading the entire file into memory;
 * instead, it operates using iterators. Therefore, it should handle even very large files with ease.
 * The JAR file is executable from the command line with a parameter specifying the path and name of the file.
 * for example: java -jar PrimesFromExcel.jar C:\Data\vzorova_data.xlsx
 */
public class Main {
    public static void main(String[] args) {
        // Check if the input file argument is provided
        if (args.length != 1) {
            System.err.println("Missing parameter. Usage: java PrimesFromExcel <data_file.xlsx>");
            System.exit(1);
        }
        String fileName = args[0];

        // Open the Excel file and create a workbook and sheet objects - using try with resource
       try(FileInputStream file = new FileInputStream(fileName);
           Workbook workbook = new XSSFWorkbook(file)){

        XSSFSheet firstSheet = (XSSFSheet) workbook.getSheetAt(0);//get a first sheet

           for (Row row : firstSheet) {
               // Get the cell in column B (index 1) of the current row
               Cell cell = row.getCell(1);

               // Validate if the cell contains a valid positive number and print it, if it's a prime number
               if (isValidNumber(cell.getStringCellValue())) {
                   int number = Integer.parseInt(cell.getStringCellValue());

                   if (isPrime(number)) {
                       System.out.println(number);
                   }
               }
           }
       } catch (IOException e){
           e.printStackTrace();
       }
    }

    /**
     * Checks if the input string represents a valid positive integer.
     *
     * @param str the input string to be validated
     * @return true if the string represents a valid positive integer, false otherwise
     */
    private static boolean isValidNumber(String str) {
        try {
            int number = Integer.parseInt(str);
            return number > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Checks if the input number is a prime number.
     *
     * @param n the input number to be checked
     * @return true if the number is a prime number, false otherwise
     */
    private static boolean isPrime(int n) {
        if (n <= 1)
            return false;

        if (n == 2 || n == 3)
            return true;

        if (n % 2 == 0 || n % 3 == 0)
            return false;

        for (int i = 5; i <= Math.sqrt(n); i = i + 6)
            if (n % i == 0 || n % (i + 2) == 0)
                return false;

        return true;
    }
}
